# Disable building of vendor boot image. Current modular boot bootloader
# implementation does not support vendor boot images.
TARGET_NO_VENDOR_BOOT := true

# vendor sepolicy
#BOARD_VENDOR_SEPOLICY_DIRS += device/qcom/gull/shared/sepolicy/vendor

# Clear vendor kernel modules list inherited from cuttlefish target.
BOARD_VENDOR_RAMDISK_KERNEL_MODULES :=

# Filter out not desired kernel command line parameter inherited from cuttlefish
# configuration.
BOARD_KERNEL_CMDLINE := $(filter-out androidboot.console=ttyS1,$(BOARD_KERNEL_CMDLINE))

# Extend kernel command line with gull specific parameters.
BOARD_KERNEL_CMDLINE += androidboot.fstab_name=fstab
BOARD_KERNEL_CMDLINE += androidboot.hardware.egl=adreno
BOARD_KERNEL_CMDLINE += androidboot.hardware.gralloc=default
BOARD_KERNEL_CMDLINE += androidboot.hardware.hwcomposer=drm_qcom
BOARD_KERNEL_CMDLINE += androidboot.selinux=permissive
BOARD_KERNEL_CMDLINE += enforcing=0
BOARD_KERNEL_CMDLINE += audit=1
BOARD_KERNEL_CMDLINE += loop.max_part=7
# This arguments duplicate arguments from trout. But it is important to keep them
# here because the order they passed is matters. They should follow after
# androidboot.hardware.egl=adreno.
BOARD_KERNEL_CMDLINE += smp
BOARD_KERNEL_CMDLINE += earlyprintk
BOARD_KERNEL_CMDLINE += androidboot.fstab_suffix=f2fs
BOARD_KERNEL_CMDLINE += androidboot.hardware=cutf_cvm
BOARD_KERNEL_CMDLINE += androidboot.lcd_density=160
BOARD_KERNEL_CMDLINE += androidboot.serialno=CUTTLEFISHCVD01
BOARD_KERNEL_CMDLINE += androidboot.setupwizard_mode=DISABLED
BOARD_KERNEL_CMDLINE += androidboot.tombstone_transmit=0
BOARD_KERNEL_CMDLINE += androidboot.cf_devcfg=1
BOARD_KERNEL_CMDLINE += androidboot.vendor.vehiclehal.server.cid=2
BOARD_KERNEL_CMDLINE += androidboot.vendor.vehiclehal.server.port=9210
BOARD_KERNEL_CMDLINE += androidboot.wifi_mac_address=40:1:2:3:4:1
BOARD_KERNEL_CMDLINE += security=selinux
BOARD_KERNEL_CMDLINE += printk.devkmsg=on
BOARD_KERNEL_CMDLINE += virtio_video.vid_nr_cam=0
BOARD_KERNEL_CMDLINE += virtio_video.mplane_cam=0

TARGET_USERIMAGES_SPARSE_F2FS_DISABLED := false
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := false

# qseecom HAL
DEVICE_PRODUCT_COMPATIBILITY_MATRIX_FILE := device/qcom/gull/shared/device_framework_matrix_product.xml
