TARGET_BOARD_INFO_FILE ?= device/qcom/gull/board-info.txt
TARGET_USERDATAIMAGE_FILE_SYSTEM_TYPE = ext4
TARGET_RECOVERY_FSTAB ?= device/qcom/gull/shared/config/fstab.ext4

# We need to point gull kernel location over external ANDROID_KERNEL_DIR variable,
# as currently we do not have perbuilt kernel available.
#ifneq ($(ANDROID_KERNEL_DIR),)
#PRODUCT_COPY_FILES += $(ANDROID_KERNEL_DIR)/dist/Image:kernel
#PRODUCT_COPY_FILES += $(ANDROID_KERNEL_DIR)/Image:kernel
#else
#$(error ANDROID_KERNEL_DIR is not set)
#endif

PRODUCT_COPY_FILES += \
	device/qcom/gull/shared/config/fstab.ext4:$(TARGET_COPY_OUT_RAMDISK)/fstab.ext4 \
	device/qcom/gull/shared/config/fstab.ext4:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.ext4 \
	device/qcom/gull/shared/config/fstab.ext4:$(TARGET_COPY_OUT_RECOVERY)/root/first_stage_ramdisk/fstab.ext4 \

# rules for gull device nodes
PRODUCT_COPY_FILES += \
	device/qcom/gull/shared/config/ueventd.rc:$(TARGET_COPY_OUT_VENDOR)/ueventd.rc \

# QTI Bluetooth HAL
PRODUCT_PACKAGES += \
	android.hardware.bluetooth@1.0-impl-qti \
	android.hardware.bluetooth@1.0-service-qti \

PRODUCT_PROPERTY_OVERRIDES += \
	vendor.qcom.bluetooth.soc=rome \

# Android Bluetooth stack configuration for QTI BT chip
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := device/qcom/gull/shared/bluetooth

# gRPC Audio HAL
PRODUCT_PACKAGES += \
	audio.primary.grpc \

PRODUCT_PROPERTY_OVERRIDES += \
	ro.hardware.audio.primary=grpc \

#MSM_AUDIO_HAL_PATH := vendor/qcom/opensource/audio-hal
#MSM_PRODUCT := msmnile_au

#PRODUCT_COPY_FILES += \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/audio_io_policy.conf:$(TARGET_COPY_OUT_VENDOR)/etc/audio_io_policy.conf \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/audio_effects.conf:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.conf \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/audio_effects.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.xml \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/mixer_paths_adp.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths_adp.xml \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/audio_tuning_mixer.txt:$(TARGET_COPY_OUT_VENDOR)/etc/audio_tuning_mixer.txt \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/sound_trigger_platform_info.xml:$(TARGET_COPY_OUT_VENDOR)/etc/sound_trigger_platform_info.xml \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/audio_platform_info.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_platform_info.xml \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/mixer_paths_custom.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths_custom.xml \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/sound_trigger_mixer_paths.xml:$(TARGET_COPY_OUT_VENDOR)/etc/sound_trigger_mixer_paths.xml \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/audio_configs.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_configs.xml \
	$(MSM_AUDIO_HAL_PATH)/configs/$(MSM_PRODUCT)/audio_configs_stock.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_configs_stock.xml \

#PRODUCT_COPY_FILES += \
	$(MSM_AUDIO_HAL_PATH)/configs/common/bluetooth_qti_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/bluetooth_qti_audio_policy_configuration.xml \
	$(MSM_AUDIO_HAL_PATH)/configs/common_au/car_audio_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/car_audio_configuration.xml

PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.audio.pro.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.pro.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.low_latency.xml \
	frameworks/av/services/audiopolicy/config/a2dp_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/a2dp_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/audio_policy_volumes.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_volumes.xml \
	frameworks/av/services/audiopolicy/config/default_volume_tables.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default_volume_tables.xml \
	frameworks/av/services/audiopolicy/config/r_submix_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/r_submix_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml \

# USBoIP
PRODUCT_PACKAGES += \
	usbip

# Test tools
PRODUCT_PACKAGES += \
	iio_generic_buffer \

# Explicitly enable default HAL implementations in order to turn off Trout HALs
# which are incompatible with Gull.
# See:  google/trout/aosp_trout_common.mk
LOCAL_AUDIOCONTROL_HAL_PRODUCT_PACKAGE = android.hardware.automotive.audiocontrol@2.0-service
LOCAL_VHAL_PRODUCT_PACKAGE = android.hardware.automotive.vehicle@2.0-service
LOCAL_DUMPSTATE_PRODUCT_PACKAGE = android.hardware.dumpstate@1.1-service.example

# Disable annoying Trout RIL HAL errors
PRODUCT_PROPERTY_OVERRIDES += \
	ro.kernel.qemu=1 \
	vendor.rild.libpath=libreference-ril.so \

# Software Update related tools
PRODUCT_PACKAGES += \
	update_engine \
	update_verifier \

# Software Update related test tools
PRODUCT_PACKAGES_DEBUG += \
	update_engine_client \
	bootctl \

# init script to create standard device name symbolic links pointing to real gvm devices
#PRODUCT_COPY_FILES += \
#	device/qcom/gull/shared/config/init.symlinks.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.symlinks.rc \

# Codec 2.0
#PRODUCT_COPY_FILES += \
#	device/qcom/gull/shared/config/media_codecs_google_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_c2.xml \
#	device/qcom/gull/shared/config/media_codecs_google_c2_audio.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_c2_audio.xml \
#	device/qcom/gull/shared/config/media_codecs_google_c2_video.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_c2_video.xml \
#	device/qcom/gull/shared/seccomp_policy/codec2.vendor.ext.policy:$(TARGET_COPY_OUT_VENDOR)/etc/seccomp_policy/codec2.vendor.ext.policy \
#	device/qcom/gull/shared/seccomp_policy/codec2.vendor.base.policy:$(TARGET_COPY_OUT_VENDOR)/etc/seccomp_policy/codec2.vendor.base.policy \

PRODUCT_PACKAGES += \
	hardware.google.media.c2@1.0-virt-video-service \
	libsfplugin_ccodec \
	libv4l2_codec2 \

PRODUCT_PROPERTY_OVERRIDES += \
	debug.stagefright.c2-poolmask=65536 \
	debug.stagefright.ccodec=1 \

PRODUCT_SOONG_NAMESPACES += \
	hardware/google/av \

DEVICE_MANIFEST_FILE += device/qcom/gull/shared/manifest.xml
DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE += device/qcom/gull/shared/vendor_framework_compatibility_matrix.xml

# WiFi packages
PRODUCT_PACKAGES += \
	hostapd \
	android.hardware.wifi@1.0-service-lazy \
	nl2vsock \
	nl80211_hwaddr \

PRODUCT_COPY_FILES += \
	device/qcom/gull/shared/config/init.vendor.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/hw/init.cutf_cvm.rc \

# In order to override vendor services which provides hidl interface to framework
# it is not enough to add "override" service property. Such entries hast to
# be parsed before original one. The only way to achieve it is to define them
# in .rc file which is parsed prior to rest of vendor services. Since init
# process reads rc files in alphabet order "0init.rc" name was chosen for wifi
# services overridings

# 01-01 00:00:09.822 0 0 I init : Parsing file /system/etc/init/wificond.rc...
# 01-01 00:00:09.873 0 0 I init : Parsing file /vendor/etc/init/0init.rc...
# 01-01 00:00:10.218 0 0 I init : Parsing file /vendor/etc/init/android.hardware.wifi@1.0-service-lazy.rc...
# 01-01 00:00:10.228 0 0 E init : /vendor/etc/init/android.hardware.wifi@1.0-service-lazy.rc: 2: Interface 'android.hardware.wifi@1.0::IWifi/default' redefined in vendor.wifi_hal_legacy but is already defined by vendor.wifi_hal_legacy
# 01-01 00:00:10.228 0 0 E init : /vendor/etc/init/android.hardware.wifi@1.0-service-lazy.rc: 1: ignored duplicate definition of service 'vendor.wifi_hal_legacy'
#PRODUCT_COPY_FILES += \
#	device/qcom/gull/shared/config/init.nl2vsock.wifi.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/0init.rc \

# misc
PRODUCT_PROPERTY_OVERRIDES += \
	log.tag.ModernMediaScanner=ERROR \

# State Manager client
PRODUCT_PACKAGES += \
	sm-client \

# EVS packages from packages/services/Car/car_product/build/car_base.mk
PRODUCT_PACKAGES += \
	evs-app-virtio \
	android.automotive.evs.manager@1.1 \
	android.hardware.automotive.evs@1.1-virtio-video \
	android.frameworks.automotive.display@1.0-service \

# Autostart EVS HAL and manager
PRODUCT_PRODUCT_PROPERTIES += persist.automotive.evs.mode=1

# hammer h264 decoder test
PRODUCT_PACKAGES += replay-h264-dump-test

#PRODUCT_COPY_FILES += \
#	device/qcom/gull/commands/video-decoder-test/carplay.h264:/data/hammer-testdata/carplay.h264 \
#	device/qcom/gull/commands/video-decoder-test/carplay.pcfg:/data/hammer-testdata/carplay.pcfg \

#add neuralnetworks for hy11 ship rules
PRODUCT_PACKAGES += android.hardware.neuralnetworks@1.0.vendor \
                    android.hardware.neuralnetworks@1.1.vendor \
                    android.hardware.neuralnetworks@1.2.vendor \
                    android.hardware.neuralnetworks@1.3.vendor

# Display packages
PRODUCT_PACKAGES += \
	hwcomposer.drm_qcom \
	android.hardware.graphics.mapper@3.0-impl-qti-display \
	android.hardware.graphics.mapper@4.0-impl-qti-display \
	vendor.qti.hardware.display.allocator@1.0-service \
	vendor.qti.hardware.display.allocator-service \
	vendor.qti.hardware.display.mapper@3.0 \
	vendor.qti.hardware.display.mapper@4.0.vendor
