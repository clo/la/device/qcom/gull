include device/qcom/gull/aosp_gull_common.mk

$(call inherit-product, device/google/trout/aosp_trout_arm64.mk)
$(call inherit-product-if-exists, $(QCPATH)/common/config/device-vendor.mk)

PRODUCT_NAME := aosp_gull_arm64
PRODUCT_DEVICE := aosp_gull_arm64
PRODUCT_MODEL := arm64 gull
TARGET_KERNEL_VERSION := 5.4
ENABLE_HYP := true
TARGET_USES_QCOM_BSP := false
TARGET_NO_TELEPHONY := true
TARGET_USES_QTIC := false
TARGET_USES_QTIC_EXTENSION := false
BOARD_USES_QCNE := false
BOARD_HAVE_QCOM_FM := false
BOARD_VENDOR_QCOM_LOC_PDK_FEATURE_SET := false
TARGET_ENABLE_QC_AV_ENHANCEMENTS := false
TARGET_FWK_SUPPORTS_AV_VALUEADDS := false
TARGET_FWK_SUPPORTS_FULL_VALUEADDS := false
BOARD_HAS_QCOM_WLAN := false
TARGET_NO_QTI_WFD := true
TARGET_BOARD_AUTO := true
TARGET_DISABLE_DASH := true
TARGET_DISABLE_QTI_VPP := false
# diag-router
TARGET_HAS_DIAG_ROUTER := true
BOARD_AVB_ENABLE := true

TARGET_HAS_GENERIC_KERNEL_HEADERS := true
#Enable llvm support for kernel
KERNEL_LLVM_SUPPORT := true
#Enable sd-llvm suppport for kernel
KERNEL_SD_LLVM_SUPPORT := false
PRODUCT_OTA_ENFORCE_VINTF_KERNEL_REQUIREMENTS := false
PRODUCT_PACKAGES += uhabtest

QCOM_BOARD_PLATFORMS += aosp_gull_arm64
###################################################################################
# This is the End of target.mk file.
# Now, Pickup other split product.mk files:
###################################################################################
# TODO: Relocate the system product.mk files pickup into qssi lunch, once it is up.
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/system/*.mk)
$(call inherit-product-if-exists, vendor/qcom/defs/product-defs/vendor/*.mk)
###################################################################################
